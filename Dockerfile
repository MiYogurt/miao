FROM node:9.2.0

RUN ["mkdir", "-p", "/usr/src/app"]

WORKDIR /usr/src/app

COPY package.json /usr/src/app/

# RUN npm i --production

RUN ["npm", "install", "--registry=https://registry.npm.taobao.org"]

# RUN yarn

COPY . /usr/src/app

EXPOSE 7001

CMD ["npm" , "run" ,"docker"]
