'use strict'
const R = require('ramda')
const chalk = require('chalk')

module.exports = appInfo => {
  const config = (exports = {})
  config.sequelize = {
    dialect: 'mysql',
    database: 'miao',
    host: 'db',
    port: '3306',
    username: 'root',
    password: 'password',
    operatorsAliases: false
  }

  config.redis = {
    client: {
      port: 6379, // Redis port
      host: 'cache', // Redis host
      password: 'password',
      db: 0
    }
  }

  return config
}

